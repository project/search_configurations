<?php

/**
 * @file
 * Alter the Single Item Export Page of Config Manager.
 */

use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 * @param string $route_name
 * @param RouteMatchInterface $route_match
 * @return string
 */

function search_configurations_help($route_name, RouteMatchInterface $route_match) {
  $output = '';
  switch($route_name) {
    case 'help.page.search_configurations':
      $output = 'This module will be helpful for searching a single configuration name in a large set of configuration names in Single Item Export Page. Happy Searching...';
  }
  return $output;
}

/**
 * Implements hook_form_alter().
 * Alter the config_single_export_form form to put an search field.
 * @param array $form
 * @param FormStateInterface $form_state
 * @param string $form_id
 */

function search_configurations_form_alter(&$form, FormStateInterface $form_state, $form_id){
  if ($form_id == 'config_single_export_form') {
    $form['#attached']['library'][] = 'search_configurations/config_setting_search';
    $form['config_type']['#weight'] = 0;
    $form['config_name']['#weight'] = 0;
    $form['search_configurations_find_config'] = [
      '#type' => 'fieldset',
      '#title' => t('Find Config Settings'),
      '#weight' => 0,
    ];
    $form['search_configurations_find_config']['find_machine_name'] = [
      '#type' => 'textfield',
      '#title' => t('Fully Qualified Config Name'),
      '#placeholder' => t('For ex: add_to_head.settings'),
      '#description' => t('After selecting the Configuration type, find config settings directly.'),
    ];
    $form['search_configurations_find_config']['actions']['find_config_settings'] = [
      '#type' => 'button',
      '#value' => t('Find'),
      '#ajax' => [
        'callback' => 'find_update_export',
        'wrapper' => 'edit-export-wrapper',
      ],
    ];
  }
}

/**
 * Handles switching the export textarea.
 */

function find_update_export($form, FormStateInterface $form_state) {
  // Determine the full config name for the selected config entity.
  if ($form_state->getValue('config_type') !== 'system.simple') {
    $definition = \Drupal::service('entity.manager')->getDefinition($form_state->getValue('config_type'));
    $name = $definition->getConfigPrefix() . '.' . $form_state->getValue('find_machine_name');
  }
  // The config name is used directly for simple configuration.
  else {
    $name = $form_state->getValue('config_name');
  }
  // Read the raw data for this config name, encode it, and display it.
  $form['export']['#value'] = Yaml::encode(\Drupal::service('config.storage')->read($name));
  $form['export']['#description'] = t('Filename: %name', ['%name' => $name . '.yml']);
  return $form['export'];
}
